const http = require('http');

const dogs = [
    {
        name: 'woof',
        age: 1,
        breed: 'dog'
    }
]

const requestListener = function (req, res) {
  res.writeHead(200);
  res.end(JSON.stringify(dogs));
}

const server = http.createServer(requestListener);
server.listen(3000, "0.0.0.0");